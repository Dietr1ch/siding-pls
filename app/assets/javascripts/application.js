// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require turbolinks
//= require_tree .
//

function randomBG(){
  var r = Math.floor(Math.random() * 192);
  var g = Math.floor(Math.random() * 192);
  var b = Math.floor(Math.random() * 192);
  var col = 'rgb('+r+','+g+','+b+')';
  document.body.style.background = col;
}

var currentTimer = null;
function stopTimer() {
  if (currentTimer != null) {
    clearInterval(currentTimer);
    currentTimer = null;
  }
}
function runTask(task, frequencyInMilliseconds) {
  var taskTime = frequencyInMilliseconds ? frequencyInMilliseconds : defaultTaskTimer;
  stopTimer();

  task();
  currentTimer = setInterval(function() {
    task();
  }, taskTime);
}



//jQuery(function ($) {
  //$(window).resize(function () {
    //$(".input-prepend, .input-append").each(function (index, group) {
      //var input = $("input", group).css('width', '');
      //$(".add-on, .btn", group).each(function () {
        //input.css('width', '-=' + $(this).outerWidth());
      //});
    //});
  //}).trigger('resize');
//});
